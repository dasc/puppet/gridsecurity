class gridsecurity (
    Array[Struct[{dn => String,
                  username => Array[String]}]] $whitelist,
) {
    file {'/etc/grid-security':
        ensure => 'directory',
        owner  => 'root',
        group  => 'root',
        mode   => '0755',
    }

    file {'/etc/grid-security/scitokens':
        ensure => 'directory',
        owner  => 'root',
        group  => 'root',
        mode   => '1777',
    }

    file {'/etc/grid-security/proxies':
        ensure => 'directory',
        owner  => 'root',
        group  => 'root',
        mode   => '1777',
    }

    file {'/etc/grid-security/blacklist':
        ensure  => 'file',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => '',
    }

    if size($whitelist) > 0 {
        file {'/etc/grid-security/whitelist':
            ensure  => 'file',
            owner   => 'root',
            group   => 'root',
            mode    => '0644',
            content => epp('gridsecurity/whitelist.epp', {
                whitelist => $whitelist,
            }),
        }
    }
}
